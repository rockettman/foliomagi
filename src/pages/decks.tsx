import * as React from 'react';

import { NextPage } from 'next';

import { DataStore } from 'aws-amplify/datastore';

import MainAppBar from 'src/components/views/MainAppBar'
import DeckSearch from 'src/components/views/DeckSearch'
import NewDeckFab from 'src/components/NewDeckFab';
import ToolbarLayout from 'src/components/layouts/ToolbarLayout';
import TopLevelPageContiner from 'src/components/TopLevelPageContainer';
import { Deck } from 'src/models';

const DecksPage: NextPage = () => {

  const [decks, setDecks] = React.useState<Deck[]>([])

  React.useEffect(() => {

    const deckSubscription = DataStore.observeQuery(Deck)
      .subscribe(snapshot => {
        if (snapshot.isSynced) setDecks(snapshot.items)
      })

    return () => deckSubscription.unsubscribe()
  }, [])

  return (
    <TopLevelPageContiner id='page-container'>
      <ToolbarLayout
        label='decks-page'
        toolbar={<MainAppBar />}
      >
        <DeckSearch decks={decks} />
        <NewDeckFab />
      </ToolbarLayout>
    </TopLevelPageContiner>
  );
}

export default DecksPage;
