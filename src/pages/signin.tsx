import React from 'react';

import { NextPage } from 'next';
import { useRouter } from 'next/router';

import { Authenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import { Hub } from 'aws-amplify/utils';

const SignInPage: NextPage = () => {

  const router = useRouter();

  Hub.listen('auth', (data) => {
    if (data.payload.event === "signedIn") {
      router.replace('/')
    }
  })

  return (
    <Authenticator />
  );
}

export default SignInPage;
