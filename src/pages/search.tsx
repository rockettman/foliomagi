import * as React from 'react';

import { NextPage } from 'next';

import TopLevelPageContiner from 'src/components/TopLevelPageContainer';
import CardSearch from 'src/components/views/CardSearch';
import NavMenu from 'src/components/views/NavMenu';
import useToggle from 'src/hooks/useToggle';

import MenuIcon from '@mui/icons-material/Menu';
import IconButton from '@mui/material/IconButton';
import Drawer from '@mui/material/Drawer';

const SearchPage: NextPage = () => {

  const [
    navDrawerIsOpen,
    openNavDrawer,
    closeNavDrawer
  ] = useToggle()

  return (
    <React.Fragment>
      <TopLevelPageContiner id='page-container'>
        <CardSearch
          actionHook={
            <IconButton onClick={openNavDrawer}>
              <MenuIcon />
            </IconButton>
          }
        />
      </TopLevelPageContiner>
      <Drawer
        anchor='left'
        open={navDrawerIsOpen}
        onClose={closeNavDrawer}
      >
        <NavMenu />
      </Drawer>
    </React.Fragment>
  );
}

export default SearchPage;