import * as React from 'react';

import Head from 'next/head';
import { AppProps } from 'next/app';

import theme from 'src/theme';
import createEmotionCache from 'src/createEmotionCache';
import { Authenticator } from '@aws-amplify/ui-react';
import { SearchIndexProvider } from 'src/contexts/searchIndex';
import 'src/styles/background.css';

import { Amplify } from 'aws-amplify';
import amplifyconfig from 'src/amplifyconfiguration.json';

import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider, EmotionCache } from '@emotion/react';

// Configure Amplify
Amplify.configure(
  amplifyconfig,
  { ssr: true },
);

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();


interface FolioMagiAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

export default function FolioMagiApp(props: FolioMagiAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>FolioMagi</title>
        <meta name="description" content="A deck building app for the Harry Potter Trading Card Game" />
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <div id="stars"></div>
        <div id="twinkling"></div>
        <Authenticator.Provider>
          <SearchIndexProvider>
            <Component {...pageProps} />
          </SearchIndexProvider>
       </Authenticator.Provider>
      </ThemeProvider>
    </CacheProvider>
  );
}
