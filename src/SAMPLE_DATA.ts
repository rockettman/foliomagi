import { Deck } from "src/models";
import { newDeck } from "src/utils";

const SAMPLE_DECK1 = Deck.copyOf(newDeck(), deck => {
  deck.name = 'Sample Deck1'
  deck.iconCard = 'QC79'
  deck.startingCharacter = 'QC18'
  deck.cards = [
    {card: 'QC8', count: 1},
    {card: 'QC16', count: 1},
    {card: 'QC19', count: 1},
    {card: 'QC55', count: 4},
    {card: 'QC60', count: 3},
    {card: 'QC64', count: 4},
    {card: 'QC69', count: 2},
    {card: 'AH1', count: 1},
    {card: 'AH4', count: 1},
    {card: 'AH23', count: 1},
    {card: 'AH24', count: 1},
    {card: 'AH35', count: 1},
    {card: 'AH45', count: 1},
    {card: 'AH54', count: 2},
    {card: 'CS3', count: 1},
    {card: 'CS106', count: 1},
    {card: 'CS112', count: 1},
    {card: 'CS135', count: 1},
    {card: 'DA34', count: 1},
    {card: 'DA35', count: 1},
    {card: 'DA45', count: 1},
    {card: 'DA52', count: 2},
    {card: 'DA66', count: 2},
  ]
  deck.sideboard = [
    {card: 'AH16', count: 1},
  ]
})

const SAMPLE_DECK2 = Deck.copyOf(newDeck(), deck => {
  deck.name = 'Sample Deck2'
  deck.startingCharacter = 'B1'
  deck.cards = [
    {card: 'QC8', count: 1},
    {card: 'QC16', count: 1},
    {card: 'QC19', count: 1},
  ]
  deck.sideboard = []
})

const SAMPLE_DECK3 = Deck.copyOf(newDeck(), deck => {
  deck.name = 'Sample Deck3'
  deck.iconCard = 'AH1'
  deck.startingCharacter = 'AH1'
  deck.cards = [
    {card: 'QC8', count: 1},
    {card: 'QC16', count: 1},
    {card: 'QC19', count: 1},
  ]
  deck.sideboard = []
})

const SAMPLE_DECKS = [
  SAMPLE_DECK1,
  SAMPLE_DECK2,
  SAMPLE_DECK3,
  newDeck(),
  newDeck(),
  newDeck(),
  newDeck()
]
export default SAMPLE_DECKS