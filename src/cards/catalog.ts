import { Document as FlexsearchDocumentIndex } from 'flexsearch';
import { Card } from './types';
import cardSets from './cardSets';


export class CardCatalog {
  readonly cardIndexMap: {[key: number]: Card} = {}
  readonly cardIdMap: {[key: string]: Card} = {}
  readonly index = new FlexsearchDocumentIndex({
    tokenize: 'forward',
    document: {
      id: 'id',
      index: [
        'name',
        'type',
        'subTypes[]',
        'rarity',
        'illustrator[]',
        'note',
        'description',
        'adventureInfo:effect',
        'adventureInfo:solution',
        'adventureInfo:reward',
        'matchInfo:toWin',
        'matchInfo:prize'
      ]
    }
  });
  readonly rarities: string[] = []
  readonly startingCharacters: Card[] = []

  constructor() {
    console.info('[CardCatalog] Create')
    let count = 0;
    const raritySet = new Set<string>()

    cardSets.forEach(set => {
      set.cards.forEach(card => {
        count = count + 1
        this.cardIndexMap[count] = card
        this.cardIdMap[card.id] = card
        raritySet.add(card.rarity)
      })
    })

    this.rarities = Array.from(raritySet).sort()

    this.startingCharacters = Object.values(this.cardIdMap).filter(card => (
      card.type === 'Character' && (
        card.subTypes.includes('Witch') ||
        card.subTypes.includes('Wizard')
      )
    )).sort((a, b) => {
      if (a.name > b.name) return 1
      else if (b.name > a.name) return -1
      else return 0
    })
  }

  async hydrate() {
    console.info('[CardCatalog] Hydrate')
    for await (const cardNum of Object.keys(this.cardIndexMap).map(Number)) {
      await this.index.addAsync(cardNum, this.cardIndexMap[cardNum])
    }
  }

  allCards() {
    return Object.values(this.cardIndexMap)
  }

  async search(query: string) {
    // Raw flexsearch results with card indexes per field
    const rawResults = await this.index.searchAsync(query, 1000);

    // Dedupe card indexes and map to Card objects
    return [...new Set(
      rawResults.flatMap(field => {
        return field.result.map(Number)
      })
    )].map(cardNum => {
      return this.cardIndexMap[cardNum]
    })
  }
}
