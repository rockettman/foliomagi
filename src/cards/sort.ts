import { Card, CardType, HPTCGSetName } from "./types"

export const SortField = {
  'Card Type': 'Card Type',
  'Lesson': 'Lesson',
  'Power Cost': 'Power Cost',
  'Provides Power Amount': 'Provides Power Amount',
  'Creature Damage': 'Creature Damage',
  'Creature Health': 'Creature Health',
  'Set': 'Set',
  'Alphabetical': 'Alphabetical'
}
export type SortField = keyof typeof SortField

export interface SortOrder {
  fields: SortField[],
  reversed: SortField[]
}

const setNumberStringToValue = (setNumberString: string): number => {
  const lastChar = setNumberString.slice(-1)
  if (isNaN(Number(lastChar))) {
    const addValue = (['a', 'b', 'c'].indexOf(lastChar) + 1) * 0.1
    return Number(setNumberString.slice(0, -1)) + addValue
  } else {
    return Number(setNumberString)
  }
}

const FieldValue: {[key in SortField]: (card: Card) => string | Number} = {
  // Provide value that will sort card based on the order card types are defined in
  'Card Type': (card: Card) => Object.keys(CardType).indexOf(card.type),
  'Lesson': (card: Card) => card.powerCost ? card.powerCost.type : '',
  'Power Cost': (card: Card) => card.powerCost ? card.powerCost.amount : 0,
  'Provides Power Amount': (card: Card) => card.providesPower ? card.providesPower.amount : 0,
  'Creature Damage': (card: Card) => card.creatureInfo ? card.creatureInfo.damage : -1,
  'Creature Health': (card: Card) => card.creatureInfo ? card.creatureInfo.health : -1,
  // Provide value that will sort card based on the order sets are defined in and it's place in that set
  'Set': (card: Card) => Object.keys(HPTCGSetName).indexOf(card.set) * 1000 + setNumberStringToValue(card.setNumber),
  'Alphabetical': (card: Card) => card.name
}

export const DefaultSortOrder: SortOrder = {
  fields: [
    'Lesson',
    'Card Type',
    'Power Cost',
    'Alphabetical'
  ],
  reversed: []
}

export function sortCards(cards: Card[], sortOrder: SortOrder = DefaultSortOrder): Card[] {
  return cards.sort(compareCards(sortOrder))
}

function compareCards(sortOrder: SortOrder) {
  return (a: Card, b: Card) => {
    let result = 0

    sortOrder.fields.some(sortField => {
      const aField = FieldValue[sortField](a)
      const bField = FieldValue[sortField](b)
      const reversed = sortOrder.reversed.includes(sortField)
      if (aField < bField) {
        result = reversed ? 1 : -1
        return true
      }
      if (bField < aField) {
        result = reversed ? -1 : 1
        return true
      }
    })

    return result
  }
}
