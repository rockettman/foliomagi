import { Card, CardType, HPTCGSetName, LessonType } from "./types"

export const ComparisonOperatorString = {
  '<': '<',
  '≤': '≤',
  '=': '=',
  '≥': '≥',
  '>': '>'
}
export type ComparisonOperatorString = keyof typeof ComparisonOperatorString

const ComparisonFunc: {[key in ComparisonOperatorString]: (a: Number, b: Number) => boolean} = {
  '<': (a, b) => a < b,
  '≤': (a, b) => a <= b,
  '=': (a, b) => a == b,
  '≥': (a, b) => a >= b,
  '>': (a, b) => a > b
}

interface ComparisonFilterOption {
  operator: ComparisonOperatorString,
  value: Number
}

interface FilterOption<T> {
  optionValue: T,
  negate: boolean
}

export interface FilterOptions {
  types: FilterOption<CardType>[];
  subTypes: FilterOption<string>[];
  lessonTypes: FilterOption<LessonType>[];
  powerCost?: ComparisonFilterOption;
  providesPowerTypes: FilterOption<LessonType>[];
  providesPowerAmount?: ComparisonFilterOption;
  creatureDmg?: ComparisonFilterOption;
  creatureHealth?: ComparisonFilterOption;
  classic: boolean;
  revival: boolean;
  sets: FilterOption<HPTCGSetName>[];
  illustrators: FilterOption<string>[];
  rarities: FilterOption<string>[];
}

export function includedOptions<T>(filter: FilterOption<T>[]): T[] {
  return filter.filter(x => !x.negate).map(x => x.optionValue)
}

export function negatedOptions<T>(filter: FilterOption<T>[]): T[] {
  return filter.filter(x => x.negate).map(x => x.optionValue)
}

export function filterIncludesAnyOf<T>(filter: FilterOption<T>[], values: T[]) {
  return includedOptions(filter).filter(x => values.includes(x)).length > 0
}

export function filterNegatesAnyOf<T>(filter: FilterOption<T>[], values: T[]) {
  return negatedOptions(filter).filter(x => values.includes(x)).length > 0
}

export const EmptyFilterOptions: FilterOptions = {
  types: [],
  subTypes: [],
  lessonTypes: [],
  providesPowerTypes: [],
  classic: true,
  revival: true,
  sets: [],
  illustrators: [],
  rarities: []
}

export function filterCards(cards: Card[], options: FilterOptions): Card[] {
  return cards.filter(card => (
    filterMultiSelect(options.types)(card.type) &&
    filterMultiSelect(options.subTypes)(card.subTypes) &&
    filterMultiSelect(options.lessonTypes)(card.powerCost?.type) &&
    filterComparison(options.powerCost, 0)(card.powerCost?.amount) &&
    filterMultiSelect(options.providesPowerTypes)(card.providesPower?.type) &&
    filterComparison(options.providesPowerAmount)(card.providesPower?.amount) &&
    filterComparison(options.creatureDmg)(card.creatureInfo?.damage) &&
    filterComparison(options.creatureHealth)(card.creatureInfo?.health) &&
    filterMultiSelect(options.sets)(card.set) &&
    filterMultiSelect(options.illustrators)(card.illustrator) &&
    filterMultiSelect(options.rarities)(card.rarity)
  ))
}

function filterMultiSelect<T>(
  filter: FilterOption<T>[]
): (value: T | T[] | undefined) => boolean {

  return (value) => {

    const arrayedValue = Array.isArray(value) ? value : [value]

    const skipFilter = filter.length == 0
    const onlyNegations = filter.length == negatedOptions(filter).length
    const valueIncludedAndNotNegated = (
      value !== undefined &&
      filterIncludesAnyOf(filter, arrayedValue) &&
      !filterNegatesAnyOf(filter, arrayedValue)
    )

    return (
      skipFilter ||
      valueIncludedAndNotNegated ||
      (
        onlyNegations &&
        !filterNegatesAnyOf(filter, arrayedValue)
      )
    )
  }
}

function filterComparison(
  filter?: ComparisonFilterOption,
  defaultValue?: Number
): (value: Number | undefined) => boolean {

  return (_value) => {
    const value = (
      _value === undefined && defaultValue !== undefined ?
      defaultValue
      : _value
    )
    const skipFilter = filter === undefined
    return (
      skipFilter ||
      (
        value !== undefined &&
        ComparisonFunc[filter.operator](value, filter.value)
      )
    )
  }
}

export function getDynamicFilterValues(cards: Card[]) {
  const subTypeSet = new Set<string>()
  const illustratorSet = new Set<string>()

  cards.forEach(card => {
    card.subTypes.forEach(subtype => subTypeSet.add(subtype))
    card.illustrator.forEach(illus => illustratorSet.add(illus))
  })

  return {
    subTypes: Array.from(subTypeSet).sort(),
    illustrators: Array.from(illustratorSet).sort()
  }
}