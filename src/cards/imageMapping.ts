import { HPTCGSetName, LessonType } from './types'


interface IconLocation {
  filled: string,
  outlined: string
}

export const LessonTypeIcons: {[key in LessonType]: IconLocation} = {
  "Care of Magical Creatures": {
    filled: '/images/lessonicons/CareOfMagicalCreatures.svg',
    outlined: '/images/lessonicons/CareOfMagicalCreaturesOutlined.svg'
  },
  Charms: {
    filled: '/images/lessonicons/Charms.svg',
    outlined: '/images/lessonicons/CharmsOutlined.svg'
  },
  Potions: {
    filled: '/images/lessonicons/Potions.svg',
    outlined: '/images/lessonicons/PotionsOutlined.svg'
  },
  Quidditch: {
    filled: '/images/lessonicons/Quidditch.svg',
    outlined: '/images/lessonicons/QuidditchOutlined.svg'
  },
  Transfiguration: {
    filled: '/images/lessonicons/Transfiguration.svg',
    outlined: '/images/lessonicons/TransfigurationOutlined.svg'
  }
}

export const SetIcons: {[key in HPTCGSetName]: IconLocation} = {
  Base: {
    filled: '/images/seticons/BaseSet.svg',
    outlined: '/images/seticons/BaseSetOutlined.svg'
  },
  "Quidditch Cup": {
    filled: '/images/seticons/QuidditchCup.svg',
    outlined: '/images/seticons/QuidditchCupOutlined.svg'
  },
  "Diagon Alley": {
    filled: '/images/seticons/DiagonAlley.svg',
    outlined: '/images/seticons/DiagonAlleyOutlined.svg'
  },
  "Adventures at Hogwarts": {
    filled: '/images/seticons/AdventuresAtHogwarts.svg',
    outlined: '/images/seticons/AdventuresAtHogwartsOutlined.svg'
  },
  "Chamber of Secrets": {
    filled: '/images/seticons/ChamberOfSecrets.svg',
    outlined: '/images/seticons/ChamberOfSecretsOutlined.svg'
  },
  "Heir of Slytherin": {
    filled: '/images/seticons/HeirOfSlytherin.svg',
    outlined: '/images/seticons/HeirOfSlytherinOutlined.svg'
  }
}