
export const CardType = {
  Character: 'Character',
  Adventure: 'Adventure',
  Location: 'Location',
  Match: 'Match',
  Item: 'Item',
  Spell: 'Spell',
  Creature: 'Creature',
  Lesson: 'Lesson'
}
export type CardType = keyof typeof CardType

export const HPTCGSetName = {
  'Base': 'Base',
  'Quidditch Cup': 'Quidditch Cup',
  'Diagon Alley': 'Diagon Alley',
  'Adventures at Hogwarts': 'Adventures at Hogwarts',
  'Chamber of Secrets': 'Chamber of Secrets',
  'Heir of Slytherin': 'Heir of Slytherin'
}
export type HPTCGSetName = keyof typeof HPTCGSetName

export const ClassicSets: HPTCGSetName[] = [
  'Base',
  'Quidditch Cup',
  'Diagon Alley',
  'Adventures at Hogwarts',
  'Chamber of Secrets'
]

export const RevivalSets: HPTCGSetName[] = [
  'Heir of Slytherin'
]

export const LessonType = {
  'Care of Magical Creatures': 'Care of Magical Creatures',
  'Charms': 'Charms',
  'Potions': 'Potions',
  'Quidditch': 'Quidditch',
  'Transfiguration': 'Transfiguration'
}
export type LessonType = keyof typeof LessonType

export type HPTCGSet = {
  readonly setName: HPTCGSetName,
  readonly releaseDate: string,
  readonly totalCards: Number,
  readonly cards: Card[]
}

export type Card = {
  readonly id: string,
  readonly name: string,
  readonly type: CardType,
  readonly subTypes: string[],
  readonly set: HPTCGSetName,
  readonly setNumber: string,
  readonly rarity: string,
  readonly illustrator: string[],
  readonly powerCost?: Lesson,
  readonly providesPower?: Lesson,
  readonly note?: string,
  readonly description?: string,
  readonly flavorText?: string,
  readonly creatureInfo?: CreatureInfo,
  readonly adventureInfo?: AdventureInfo,
  readonly matchInfo?: MatchInfo
}

export interface Lesson {
  readonly type: LessonType,
  readonly amount: Number
}

export interface CreatureInfo {
  readonly damage: Number,
  readonly health: Number
}

export interface AdventureInfo {
  readonly effect: string,
  readonly solution: string,
  readonly reward: string
}

export interface MatchInfo {
  readonly toWin: string,
  readonly prize: string
}