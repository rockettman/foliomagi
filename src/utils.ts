import { CardCount, CardId, Deck } from "src/models";


export function deckUrl(username?: string, deckId?: string) {
  return `/deck/${username}/${deckId}`
}

export function newDeck() {
  return new Deck({
    name: 'New Deck',
    startingCharacter: 'B9',
    lessons: {
      careOfMagicalCreatures: 0,
      charms: 0,
      potions: 0,
      quidditch: 0,
      transfiguration: 0
    },
    cards: [],
    sideboard: []
  })
}

export function increaseCardCount(cardList: CardCount[], cardId: CardId) {

  if (cardList.find(cc => cc.card === cardId) !== undefined) {
    return cardList.map(cardCount => ({
      ...cardCount,
      count: cardCount.card === cardId ? cardCount.count + 1 : cardCount.count
    }))
  } else {
    return cardList.concat({ card: cardId, count: 1 })
  }
}

export function decreaseCardCount(cardList: CardCount[], cardId: CardId) {

  return cardList
    .map(cardCount => ({
      ...cardCount,
      count: cardCount.card === cardId ? cardCount.count - 1 : cardCount.count
    }))
    .filter(cardCount => cardCount.count > 0)
}
