import React from 'react';

import Image, { ImageProps } from 'next/image'

import { Card, CardType, HPTCGSetName } from 'src/cards';
import CardBack from '/public/images/cards/cardBack.png'

import { styled } from '@mui/material/styles';

const SetToPathMap: {[key in HPTCGSetName]: string} = {
  'Base': 'base',
  'Quidditch Cup': 'qc',
  'Diagon Alley': 'da',
  'Adventures at Hogwarts': 'ah',
  'Chamber of Secrets': 'cs',
  'Heir of Slytherin': 'hs'
}

const VerticalCardImage = styled(Image)({
  objectFit: 'contain',
})

const HorizontalCardImage = styled(VerticalCardImage)({
  transform: 'rotate(90deg)',
})

interface CardImageProps extends Partial<ImageProps> {
  card?: Card,
  forceVertical?: boolean
}

export default function CardImage(props: CardImageProps) {
  const {card, forceVertical, ...other} = props

  const imageProps: ImageProps = React.useMemo(() => {
    if (card) {
      return {
        src: `/images/cards/${SetToPathMap[card.set]}/${card.id}.webp`,
        alt: card.name
      }
    } else {
      return {
        src: CardBack,
        alt: 'card back'
      }
    }
  }, [card])

  if (card === undefined || card.type === CardType.Spell || forceVertical) {
    return (
      <VerticalCardImage
        {...other}
        {...imageProps}
      />
    )
  } else {
    return (
      <HorizontalCardImage
        {...other}
        {...imageProps}
      />
    )
  }
}