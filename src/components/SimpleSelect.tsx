import React from 'react'

import Select, { SelectProps } from "@mui/material/Select"
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'

type SimpleSelectProps<T> = Partial<SelectProps<T>> & {
  label: string,
  options: T[],
  value: T
}

export default function SimpleSelect<T extends React.ReactNode>(props: SimpleSelectProps<T>) {
  const {label, options, value, ...other} = props

  return (
    <FormControl fullWidth>
      <InputLabel id={`simple-select-label-${label}`}>{label}</InputLabel>
      <Select
        {...other}
        labelId={`simple-select-label-${label}`}
        id={`simple-select-${label}`}
        value={value}
        label={label}
      >
        {options.map(option => {
          return (
            <MenuItem key={`${option}`} value={`${option}`}>{option}</MenuItem>
          )
        })}
      </Select>
    </FormControl>
  )
}
