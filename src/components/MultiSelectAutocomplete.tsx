import React from 'react'

import CustomIcon from 'src/components/CustomIcon';

import Autocomplete, { AutocompleteProps } from '@mui/material/Autocomplete';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import BlockIcon from '@mui/icons-material/Block';
import Chip from '@mui/material/Chip';

interface MultiSelectAutocompleteProps<T> extends Partial<AutocompleteProps<T, true, undefined, undefined>> {
  label: string;
  options: T[];
  disabledOptions?: T[];
  value: T[];
  negated: T[];
  onNegate: (value: T) => void;
  chipOnDelete: (value: T) => (event: any) => void;
  getIconPaths?: (value: T) => {filled: string, outlined: string};
}

export default function MultiSelectAutocomplete<T extends React.ReactNode>(props: MultiSelectAutocompleteProps<T>) {
  const {
    label,
    options,
    disabledOptions = [],
    value,
    negated,
    onNegate,
    chipOnDelete,
    getIconPaths,
    ...other
  } = props

  const useCustomIcon = getIconPaths !== undefined

  return (
    <Autocomplete
      {...other}
      id={`filter-input-${label}`}
      options={options}
      multiple
      disableCloseOnSelect
      value={value}
      getOptionDisabled={option => disabledOptions.includes(option)}
      renderTags={(value, getTagProps) => (
        value.map((selected, index) => (
          <Chip
            {...getTagProps({index})}
            key={`${selected}`}
            label={`${selected}`}
            onDelete={chipOnDelete(selected)}
            onMouseDown={(event) => {
              event.stopPropagation();
            }}
            {...(
              negated.includes(selected) ?
              {style: {backgroundColor: 'red'}}
              : {}
            )}
          />
        ))
      )}
      renderOption={(renderProps, option, { selected }) => (
        <ListItem
          {...renderProps}
          dense
          secondaryAction={
            <IconButton onClick={(event) => {
              event.stopPropagation()
              onNegate(option)
            }}>
              <BlockIcon style={{color: negated.includes(option) ? 'red' : 'grey'}} />
            </IconButton>
          }
        >
          <ListItemIcon>
            <Checkbox
              edge="start"
              checked={selected && !negated.includes(option)}
              disableRipple
              icon={useCustomIcon ?
                CustomIcon(getIconPaths(option).outlined, `${option}`)
                : undefined
              }
              checkedIcon={useCustomIcon ?
                CustomIcon(getIconPaths(option).filled, `selected ${option}`)
                : undefined
              }
            />
          </ListItemIcon>
          {option}
        </ListItem>
      )}
      fullWidth
      sx={{
        marginTop: (theme) => theme.spacing(1),
        marginBottom: (theme) => theme.spacing(3)
      }}
      renderInput={(params) => (
        <TextField {...params} label={label} />
      )}
    />
  );
}
