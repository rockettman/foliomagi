
import { styled } from "@mui/material/styles"
import Stack from "@mui/material/Stack"

const VerticalLayoutBox = styled(Stack)({
  flex: 1,
  minHeight: '100%',
  maxHeight: '100%'
})

export default VerticalLayoutBox
