import { Card } from "src/cards";
import CardImage from "src/components/CardImage";

import { styled, SxProps, Theme } from "@mui/material/styles";
import Box, { BoxProps } from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";

const StyledCardImage = styled(CardImage)({
  margin: 'auto',
  position: 'absolute',
  top: 0,
  left: 0,
  bottom: 0,
  right: 0,
  maxWidth: '100%',
  maxHeight: '100%',
})

interface SquareCardImageProps extends Partial<BoxProps> {
  card?: Card,
  onClick?: () => void,
  priority?: boolean,
}

export default function SquareCardImage(props: SquareCardImageProps) {
  const {card, onClick, priority, ...other} = props

  const ContainerSxProps: SxProps<Theme> = {
    ...other.sx,
    position: 'relative'
  }

  return (
    <Box id={`square-card-image-container-${card?.id}`}
      {...other}
      sx={ContainerSxProps}
    >
      <Box sx={{
        paddingTop: '100%',
        display: 'block',
        position: 'relative'
      }}>
        <StyledCardImage
          card={card}
          layout='fill'
          priority={priority ?? false}
        />
      </Box>
      {onClick &&
        <ButtonBase
          onClick={onClick}
          sx={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: (theme) => theme.spacing()
          }}
        />
      }
    </Box>
  )
}
