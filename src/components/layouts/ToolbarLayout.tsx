import React from 'react'

import VerticalLayoutBox from 'src/components/VerticalLayoutBox'

import Box from '@mui/material/Box'

export default function ToolbarLayout(props: {
  label: string,
  toolbar: JSX.Element,
  children: React.ReactNode
}) {
  const {label, toolbar, children} = props

  return (
    <VerticalLayoutBox id={label}>
      <Box id={`toolbar-${label}`}
        sx={{ flex: 'none' }}
      >
        {toolbar}
      </Box>
      <Box id={`contents-${label}`}
        sx={{
          display: 'flex',
          flex: 1,
          minHeight: 0
        }}
      >
        {children}
      </Box>
    </VerticalLayoutBox>
  )
}