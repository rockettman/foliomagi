import React from 'react'

import ToolbarLayout from 'src/components/layouts/ToolbarLayout';

import Box, { BoxProps } from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Stack from '@mui/material/Stack';


interface TabPanelProps extends Partial<BoxProps> {
  label: string;
  active: boolean;
  children?: React.ReactNode;
}

function TabPanel(props: TabPanelProps) {
  const { label, active, children, ...other } = props;

  return (
    <Box id={`tabpanel-${label}`}
      {...other}
      role="tabpanel"
      hidden={!active}
      aria-labelledby={`tab-${label}`}
      sx={{
        flex: 1,
        minHeight: 0
      }}
    >
      <Stack sx={{
        width: '100%',
        height: '100%'
      }}>
        {children}
      </Stack>
    </Box>
  );
}

function a11yProps(group: string, label: string) {
  return {
    id: `tab-${group}-${label}`,
    'aria-controls': `tabpanel-${group}-${label}`,
  }
}

export default function TabLayout(props: {
  label: string,
  tabs: {label: string, contents: React.ReactNode}[],
  onTabChange?: (newTab: number) => void
}) {
  const {label, tabs, onTabChange} = props
  const [activeTab, setActiveTab] = React.useState(0);

  const handleTabChange = (event: React.SyntheticEvent, value: number) => {
    setActiveTab(value)
    if (onTabChange !== undefined) onTabChange(value)
  }

  return (
    <ToolbarLayout
      label={label}
      toolbar={
        <Box id={`tabs-container-${label}`}
          sx={{
            flex: 'none',
            borderBottom: 1,
            borderColor: 'divider'
          }}
        >
          <Tabs
            value={activeTab}
            onChange={handleTabChange}
            variant='fullWidth'
            aria-label={`tabs-${label}`}
          >
            {tabs.map(tab => (
              <Tab label={tab.label} {...a11yProps(label, tab.label)} key={tab.label} />
            ))}
          </Tabs>
        </Box>
      }
    >
      <Box id={`tabpanels-container-${label}`}
        sx={{
          display: 'flex',
          flex: 1,
          minHeight: 0
        }}
      >
        {tabs.map((tab, i) => (
          <TabPanel label={`${label}-${tab.label}`} active={activeTab == i} key={tab.label}>
            {tab.contents}
          </TabPanel>
        ))}
      </Box>
    </ToolbarLayout>
  )
}