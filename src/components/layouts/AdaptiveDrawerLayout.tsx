import React from 'react'

import PersistentDrawer from 'src/components/layouts/PersistentDrawer'
import TemporaryDrawer from 'src/components/layouts/TemporaryDrawer'

import { useMediaQuery, useTheme } from '@mui/material'
import Box from '@mui/material/Box'


export default function AdaptiveDrawerLayout(props: {
  label: string,
  anchor: 'right' | 'left',
  open: boolean,
  drawerContents: React.ReactNode,
  onClose?: ((event: any) => void) | undefined,
  children: React.ReactNode,
}) {
  const {label, anchor, open, children, drawerContents, onClose} = props

  const theme = useTheme()
  const isWidescreen = useMediaQuery(theme.breakpoints.up('md'))

  return (
    <Box id={label}
      sx={{
        display: 'flex',
        flex: 1,
        width: '100%',
        minHeight: 0,
        maxHeight: '100%'
      }}
    >
      {isWidescreen ?
        <PersistentDrawer
          anchor={anchor}
          open={open}
          drawerContents={drawerContents}
        >
          {children}
        </PersistentDrawer>
        :
        <React.Fragment>
          {children}
          <TemporaryDrawer
            anchor={anchor}
            open={open}
            drawerContents={drawerContents}
            onClose={onClose}
          />
        </React.Fragment>
      }
    </Box>
  )
}