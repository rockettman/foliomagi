import React from 'react'

import { useTheme } from '@mui/material'
import useMediaQuery from '@mui/material/useMediaQuery'
import Stack from '@mui/material/Stack'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'


export default function PersistentDrawer(props: {
  anchor: 'right' | 'left',
  open: boolean,
  drawerContents: React.ReactNode,
  children: React.ReactNode,
}) {
  const {anchor, open, children, drawerContents} = props

  const theme = useTheme()
  const isLargeScreen = useMediaQuery(theme.breakpoints.only('lg'))
  const isXLargeScreen = useMediaQuery(theme.breakpoints.only('xl'))

  const drawerWidth = (() => {
    if (isXLargeScreen) return '20%'
    if (isLargeScreen) return '30%'
    return '40%'
  })()

  return (
    <Stack
      direction={anchor === 'left' ? 'row' : 'row-reverse'}
      flex={1}
    >
      <Paper
        hidden={!open}
        sx={{
          flex: 'none',
          width: drawerWidth,
          backgroundColor: (theme) => theme.palette.grey[900],
        }}
      >
        {drawerContents}
      </Paper>
      <Box sx={{
        display: 'flex',
        flex: 1,
        width: 0
      }}>
        {children}
      </Box>
    </Stack>
  )
}