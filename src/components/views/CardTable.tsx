import React from 'react'

import { Lesson, LessonTypeIcons } from "src/cards";
import { CardCount } from 'src/models';
import { SearchIndexContext } from 'src/contexts/searchIndex';
import CustomIcon from "src/components/CustomIcon";

import { DataGrid, GridColDef, GridValueFormatterParams } from "@mui/x-data-grid";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";

const CardTableColumns: GridColDef<CardTableRow>[] = [
  {
    field: 'count',
    type: 'number',
    headerName: '#',
    width: 50,
    valueFormatter: (params: GridValueFormatterParams<number>) => {
      return `x ${params.value}`
    },
  },
  {
    field: 'powerCost',
    headerName: 'Cost',
    width: 60,
    renderCell: (params) => {
      if (params.row.powerCost !== undefined) {
        const icon = LessonTypeIcons[params.row.powerCost.type].filled
        return (
          <Stack direction='row' spacing={1}>
            {CustomIcon(icon, `${params.value.type} lesson`)}
            <Typography>{params.value.amount}</Typography>
          </Stack>
        )
      }
      return undefined
    },
  },
  {
    field: 'name',
    headerName: 'Name',
    flex: 1
  }
];

interface CardTableRow {
  id: string;
  name: string;
  count: number;
  powerCost?: Lesson;
}

export default function CardTable(props: {
  cardCounts: CardCount[]
}) {
  const {cardCounts} = props

  const {cardCatalog} = React.useContext(SearchIndexContext)

  const rows = React.useMemo(() => {

    const rowObjectFromCardCount = (cardCount: CardCount): CardTableRow => {
      const card = cardCatalog!.cardIdMap[cardCount.card]
      return {
        id: card.id,
        name: card.name,
        count: cardCount.count,
        powerCost: card.powerCost
      }
    }

    if (cardCatalog) {
      return cardCounts.map(rowObjectFromCardCount)
    } else {
      return []
    }

  }, [cardCounts, cardCatalog])

  return (
    <DataGrid
      rows={rows}
      columns={CardTableColumns}
      autoHeight
      density='compact'
      hideFooter
      disableColumnMenu
      sx={{ marginTop: (theme) => theme.spacing() }}
    />
  )
}
