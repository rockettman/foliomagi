import React from 'react'

import { CardCatalog } from 'src/cards'
import SearchBox from 'src/components/SearchBox'

import { useTheme } from '@mui/material';
import Toolbar from '@mui/material/Toolbar';


interface SearchToolbarProps {
  placeholder: string,
  catalog?: CardCatalog,
  resultsCallback: (results: any) => void,
  leftAction?: JSX.Element,
  rightAction?: JSX.Element,
  elevate: boolean
}

export default function SearchToolbar(props: SearchToolbarProps) {
  const {
    placeholder,
    catalog,
    resultsCallback,
    leftAction,
    rightAction,
    elevate
  } = props

  const theme = useTheme()

  return (
    <React.Fragment>
      <Toolbar
        sx={{
          ...theme.mixins.toolbar,
          flexShrink: 0,
          flexWrap: 'wrap',
          position: 'sticky',
          paddingX: (theme) => theme.spacing(),
          backgroundColor: elevate ? theme.palette.grey[900] : 'transparent',
          borderBottom: elevate ? `1px solid ${theme.palette.divider}` : undefined
        }}
      >
        {leftAction}
        <SearchBox
          placeholder={placeholder}
          catalog={catalog}
          resultsCallback={resultsCallback}
        />
        {rightAction}
      </Toolbar>
    </React.Fragment>
  )
}