import React from 'react'

import { Card } from 'src/cards'
import { CardId } from 'src/models'
import CardListModal from 'src/components/views/CardListModal'
import { SelectCardUnderbar } from 'src/components/CardListUnderbars'

export default function ChooseCardModal(props: {
  cards: Card[],
  currentSelection?: CardId,
  open: boolean,
  onSelect: (id: CardId) => void,
  onClose: () => void
}) {
  const {cards, currentSelection, open, onSelect, onClose} = props

  return (
    <CardListModal
      cards={cards}
      initialCardShown={currentSelection}
      open={open}
      onClose={onClose}
      actions={SelectCardUnderbar({
        currentSelection: currentSelection,
        onSelect: onSelect,
        onCancel: onClose
      })}
    />
  )
}