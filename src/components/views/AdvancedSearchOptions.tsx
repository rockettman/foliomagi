import React from 'react';

import ToolbarLayout from 'src/components/layouts/ToolbarLayout';
import TabLayout from 'src/components/layouts/TabLayout';

import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';


const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));


export default function AdvancedSearchOptions(props: {
  closeCallback: React.MouseEventHandler<HTMLButtonElement>,
  onReset: () => void,
  filterMenu: JSX.Element,
  sortMenu: JSX.Element
}) {

  return (
    <ToolbarLayout
      label='advanced-search-menu'
      toolbar={
        <React.Fragment>
          <DrawerHeader>
            <IconButton onClick={props.closeCallback}>
              <ChevronRightIcon />
            </IconButton>
            <Box flexGrow={1} />
            <Button
              color='secondary'
              variant='text'
              onClick={() => props.onReset()}
            >
              Reset
            </Button>
          </DrawerHeader>
          <Divider />
        </React.Fragment>
      }
    >
      <TabLayout
        label='advanced-search'
        tabs={[
          {label: 'Filter', contents: props.filterMenu},
          {label: 'Sort', contents: props.sortMenu}
        ]}
      />
    </ToolbarLayout>
  )
}