import React from 'react'

import { useRouter } from 'next/router'

import { DataStore } from 'aws-amplify/datastore'

import { CardId, Deck } from 'src/models'
import { SearchIndexContext } from 'src/contexts/searchIndex'
import { useAuthenticator } from '@aws-amplify/ui-react';
import { deckUrl, newDeck } from 'src/utils'
import ChooseCardModal from 'src/components/views/ChooseCardModal'
import useClickOutside from 'src/hooks/useClickOutside'
import SquareCardImage from 'src/components/SquareCardImage'
import useToggle from 'src/hooks/useToggle'

import Stack from '@mui/material/Stack'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import IconButton from '@mui/material/IconButton'
import PhotoIcon from '@mui/icons-material/Photo';
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import DeleteIcon from '@mui/icons-material/Delete'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogActions from '@mui/material/DialogActions'
import Button from '@mui/material/Button'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import MoreVertIcon from '@mui/icons-material/MoreVert'


export default function DeckListItem(props: {
  deck: Deck
}) {
  const {deck} = props

  const router = useRouter()

  const {cardCatalog} = React.useContext(SearchIndexContext)
  const { user } = useAuthenticator(context => [context.user]);

  const routeToDeckPage = () => router.push(deckUrl(user?.username, deck.id))

  const [
    actionOverlayOpen,
    openActionOverlay,
    closeActionOverlay
  ] = useToggle()

  const contentRef = React.createRef()
  useClickOutside(contentRef, closeActionOverlay)

  const DeckCards = React.useMemo(() => {
    try {
      return [deck.startingCharacter]
        .concat(deck.cards.map(cc => cc.card))
        .map(cardId => cardCatalog!.cardIdMap[cardId])
    } catch {
      return []
    }
  }, [deck, cardCatalog])

  const [
    iconCardModalIsOpen,
    openIconCardModal,
    closeIconCardModal
  ] = useToggle()

  const setIconCard = (id: CardId) => {
    DataStore.save(Deck.copyOf(deck, updated => {
      updated.iconCard = id
    }))
      .then(closeIconCardModal)
  }

  const iconCard = React.useMemo(() => {
    return deck.iconCard && cardCatalog ? cardCatalog.cardIdMap[deck.iconCard] : undefined
  }, [deck, cardCatalog])

  const duplicateDeck = async () => {
    await DataStore.save(new Deck({
      name: `Copy - ${deck.name}`,
      iconCard: deck.iconCard,
      startingCharacter: deck.startingCharacter,
      lessons: deck.lessons,
      cards: deck.cards,
      sideboard: deck.sideboard,
    }))
  }

  const [
    confirmDeleteDialogIsOpen,
    openConfirmDeleteDialog,
    closeConfirmDeleteDialog
  ] = useToggle()

  const deleteDeck = () => {
    DataStore.delete(deck)
      .then(closeConfirmDeleteDialog)
  }

  const ActionOverlay = (
    <Paper sx={{
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      backgroundColor: '#000000AA'
    }}>
      <Stack height='100%' justifyContent='space-evenly'>
        <IconButton onClick={openIconCardModal}>
          <PhotoIcon />
        </IconButton>
        <IconButton onClick={duplicateDeck}>
          <ContentCopyIcon />
        </IconButton>
        <IconButton onClick={openConfirmDeleteDialog}>
          <DeleteIcon htmlColor='red' />
        </IconButton>
      </Stack>
    </Paper>
  )

  const ChooseIconCardModal = (
    <ChooseCardModal
      cards={DeckCards}
      currentSelection={deck.iconCard as CardId}
      open={iconCardModalIsOpen}
      onSelect={setIconCard}
      onClose={closeIconCardModal}
    />
  )

  const ConfirmDeleteDialog = (
    <Dialog
      open={confirmDeleteDialogIsOpen}
      onClose={closeConfirmDeleteDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Delete?
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {deck.name}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeConfirmDeleteDialog}
          sx={{ color: 'gray' }}
        >
          Cancel
        </Button>
        <Button onClick={deleteDeck}
          autoFocus
          sx={{ color: 'red' }}
        >
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  )

  const ItemContents = (
    <Box id={`deck-list-item-contents-${deck.id}`}
      ref={contentRef}
      sx={{
        position: 'relative'
      }}
    >
      <Card sx={{
        backgroundColor: '#12121275'
      }}>
        <CardHeader
          title={deck.name}
          titleTypographyProps={{
            variant: 'subtitle2'
          }}
          action={
            <IconButton onClick={openActionOverlay}>
              <MoreVertIcon />
            </IconButton>
          }
        />
        <CardContent>
          <SquareCardImage
            card={iconCard}
            onClick={actionOverlayOpen ? undefined : routeToDeckPage}
          />
        </CardContent>
      </Card>
      {actionOverlayOpen && ActionOverlay}
    </Box>
  )

  return (
    <React.Fragment>
      {ItemContents}
      {ChooseIconCardModal}
      {ConfirmDeleteDialog}
    </React.Fragment>
  )
}
