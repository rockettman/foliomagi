import React from 'react';

import {
  Card,
  CardType,
  ClassicSets,
  ComparisonOperatorString,
  FilterOptions,
  getDynamicFilterValues,
  HPTCGSetName,
  includedOptions,
  LessonType,
  LessonTypeIcons,
  negatedOptions,
  RevivalSets,
  SetIcons
} from 'src/cards';
import { SearchIndexContext } from 'src/contexts/searchIndex';
import CustomIcon from 'src/components/CustomIcon';
import SimpleSelect from 'src/components/SimpleSelect';
import MultiSelect from 'src/components/MultiSelect';
import MultiSelectAutocomplete from 'src/components/MultiSelectAutocomplete';

import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import { SelectChangeEvent } from '@mui/material/Select';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';


type MultiSelectField =
  | 'types'
  | 'subTypes'
  | 'sets'
  | 'illustrators'
  | 'rarities'
type LessonTypeField =
  | 'lessonTypes'
  | 'providesPowerTypes'
type ComparisonField =
  | 'powerCost'
  | 'providesPowerAmount'
  | 'creatureDmg'
  | 'creatureHealth'

export default function FilterOptionsMenu(props: {
  cards: Card[],
  filterOptions: FilterOptions,
  setFilterOptions: (newFilterOptions: FilterOptions) => void
}) {
  const {cards, filterOptions, setFilterOptions} = props

  const theme = useTheme();
  const {cardCatalog} = React.useContext(SearchIndexContext)

  const rarities = React.useMemo(() => {
    return cardCatalog ? cardCatalog.rarities : []
  }, [cardCatalog])

  const dynamicFilterValues = getDynamicFilterValues(cards)

  const handleMultiSelectChange = (key: MultiSelectField) => {
    return (event: SelectChangeEvent<string[]>) => {
      const selected = event.target.value as string[]
      setFilterOptions({
        ...filterOptions,
        [key]: selected.map(value => {
          const current = filterOptions[key].find(x => x.optionValue === value)
          return current !== undefined ? current : {optionValue: value, negate: false}
        })
      })
    };
  }

  const handleMultiSelectAutocompleteChange = (key: MultiSelectField) => {
    return (event: any, newValue: string[] | null) => {
      const selected = newValue ? newValue : []
      setFilterOptions({
        ...filterOptions,
        [key]: selected.map(value => {
          const current = filterOptions[key].find(x => x.optionValue === value)
          return current !== undefined ? current : {optionValue: value, negate: false}
        })
      })
    };
  }

  const handleNegate = (key: MultiSelectField) => {
    return (value: string) => {
      const current = filterOptions[key].find(x => x.optionValue === value)
      const theRest = filterOptions[key].filter(x => x.optionValue !== value)
      setFilterOptions({
        ...filterOptions,
        [key]: current?.negate ? theRest : [...theRest, {optionValue: value, negate: true}]
      })
    }
  }

  const handleChipDelete = (key: MultiSelectField) => {
    return (value: string) => {
      return (event: any) => {
        setFilterOptions({
          ...filterOptions,
          [key]: filterOptions[key].filter(x => x.optionValue !== value)
        })
      }
    }
  }

  const handleLessonTypeChange = (key: LessonTypeField, lesson: LessonType) => {
    return (event: any, checked: boolean) => {
      const lessons = filterOptions[key]
      setFilterOptions({
        ...filterOptions,
        [key]: checked ?
          [...lessons, {optionValue: lesson, negate: false}]
          : lessons.filter(x => x.optionValue !== lesson)
      })
    };
  }

  const handleComparisonOpChange = (key: ComparisonField) => {
    return (event: SelectChangeEvent<string>) => {
      if (event.target.value === '') {
        setFilterOptions({
          ...filterOptions,
          [key]: undefined
        })
      } else {
        setFilterOptions({
          ...filterOptions,
          [key]: {
            ...(
              filterOptions[key] !== undefined ?
              filterOptions[key]!
              : {value: 0}
            ),
            operator: event.target.value
          }
        })
      }
    }
  }

  const handleComparisonValueChange = (key: ComparisonField) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterOptions({
        ...filterOptions,
        [key]: {
          ...(
            filterOptions[key] !== undefined ?
            filterOptions[key]!
            : {operator: ComparisonOperatorString['=']}
          ),
          value: event.target.value
        }
      })
    }
  }

  const LessonTypeInput = (key: LessonTypeField) => {
    return (
      <Box
        sx={{
          marginTop: (theme) => theme.spacing(1),
          marginBottom: (theme) => theme.spacing(2)
        }}
      >
        <Grid container alignItems='center' justifyContent='space-around' wrap='nowrap'>
          {Object.keys(LessonType).map(lessonKey => {
            const lesson = lessonKey as LessonType
            return (
              <Grid item key={lessonKey}>
                <Checkbox
                  icon={CustomIcon(LessonTypeIcons[lesson].outlined, lesson)}
                  checkedIcon={CustomIcon(LessonTypeIcons[lesson].filled, `selected ${lesson}`)}
                  checked={includedOptions(filterOptions[key]).includes(lesson)}
                  onChange={handleLessonTypeChange(key, lesson)}
                />
              </Grid>
            )
          })}
        </Grid>
      </Box>
    )
  }

  const ComparisonFieldInput = (key: ComparisonField, valueLabel: string) => {
    return (
      <Box
        sx={{
          marginTop: (theme) => theme.spacing(1),
          marginBottom: (theme) => theme.spacing(3)
        }}
      >
        <Grid container spacing={1} alignItems='center' justifyContent='space-around'>
          <Grid item xs={6}>
            <SimpleSelect
              label='Operator'
              options={[''].concat(Object.keys(ComparisonOperatorString))}
              value={filterOptions[key] !== undefined ?
                filterOptions[key]!.operator
                : ''
              }
              onChange={handleComparisonOpChange(key)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              label={valueLabel}
              type='number'
              value={filterOptions[key] !== undefined ?
                filterOptions[key]!.value
                : ''
              }
              onChange={handleComparisonValueChange(key)}
              inputProps={{
                min: 0
              }}
            />
          </Grid>
        </Grid>
      </Box>
    )
  }

  const RulesetFilters = (
    <Box
      sx={{
        marginTop: (theme) => theme.spacing(1),
        marginBottom: (theme) => theme.spacing(2)
      }}
    >
      <Grid container alignItems='center' justifyContent='space-around'>
        <Grid item xs={6}>
          <FormControlLabel
            value="classic"
            control={
              <Switch
                checked={filterOptions.classic}
                onChange={(event, checked) => {
                  setFilterOptions({
                    ...filterOptions,
                    // negate each classic set when switch toggled off and clear when switch toggled on
                    sets: filterOptions.sets.filter(
                        s => !ClassicSets.includes(s.optionValue)
                      ).concat(ClassicSets.flatMap(
                        set => checked ? [] : {optionValue: set, negate: true}
                      )),
                    classic: checked
                  })
                }}
              />
            }
            label="Classic"
            labelPlacement="start"
          />
        </Grid>
        <Grid item xs={6}>
          <FormControlLabel
            value="revival"
            control={
              <Switch
                checked={filterOptions.revival}
                onChange={(event, checked) => {
                  setFilterOptions({
                    ...filterOptions,
                    // negate each revival set when switch toggled off and clear when switch toggled on
                    sets: filterOptions.sets.filter(
                        s => !RevivalSets.includes(s.optionValue)
                      ).concat(RevivalSets.flatMap(
                        set => checked ? [] : {optionValue: set, negate: true}
                      )),
                    revival: checked
                  })
                }}
              />
            }
            label="Revival"
            labelPlacement="start"
          />
        </Grid>
      </Grid>
    </Box>
  )

  return (
    <Box sx={{
      maxHeight: '100%',
      overflow: 'auto',
      padding: theme.spacing()
    }}>

      {/* Basic Filters */}
      <MultiSelect
        label='Card Type'
        options={Object.keys(CardType)}
        value={filterOptions.types.map(x => x.optionValue)}
        negated={negatedOptions(filterOptions.types)}
        onChange={handleMultiSelectChange('types')}
        onNegate={handleNegate('types')}
        chipOnDelete={handleChipDelete('types')}
      />
      <MultiSelectAutocomplete
        label='Subtype'
        options={dynamicFilterValues.subTypes}
        value={filterOptions.subTypes.map(x => x.optionValue)}
        negated={negatedOptions(filterOptions.subTypes)}
        onChange={handleMultiSelectAutocompleteChange('subTypes')}
        onNegate={handleNegate('subTypes')}
        chipOnDelete={handleChipDelete('subTypes')}
      />

      <Divider textAlign='left'>Lesson Type</Divider>
      {LessonTypeInput('lessonTypes')}

      <Divider textAlign='left'>Power Cost</Divider>
      {ComparisonFieldInput('powerCost', 'Cost')}

      <Divider textAlign='left'>Provides Power</Divider>
      {LessonTypeInput('providesPowerTypes')}

      <Divider textAlign='left'>Provides Power Amount</Divider>
      {ComparisonFieldInput('providesPowerAmount', 'Amount')}

      <Divider textAlign='left'>Creature Damage</Divider>
      {ComparisonFieldInput('creatureDmg', 'Value')}

      <Divider textAlign='left'>Creature Health</Divider>
      {ComparisonFieldInput('creatureHealth', 'Value')}

      <Divider textAlign='left'>Extras</Divider>
      {RulesetFilters}
      <MultiSelect
        label='Set'
        options={Object.keys(HPTCGSetName)}
        value={filterOptions.sets.map(x => x.optionValue)}
        negated={negatedOptions(filterOptions.sets)}
        disabledOptions={
          // Add classic and/or revival sets to diabled list based on switch status
          (!filterOptions.classic ? ClassicSets : []).concat(
            !filterOptions.revival ? RevivalSets : []
          )
        }
        getIconPaths={(set) => {
          return SetIcons[set as HPTCGSetName]
        }}
        onChange={handleMultiSelectChange('sets')}
        onNegate={handleNegate('sets')}
        chipOnDelete={handleChipDelete('sets')}
      />
      <MultiSelectAutocomplete
        label='Illustrator'
        options={dynamicFilterValues.illustrators}
        value={filterOptions.illustrators.map(x => x.optionValue)}
        negated={negatedOptions(filterOptions.illustrators)}
        onChange={handleMultiSelectAutocompleteChange('illustrators')}
        onNegate={handleNegate('illustrators')}
        chipOnDelete={handleChipDelete('illustrators')}
      />
      <MultiSelect
        label='Rarity'
        options={rarities}
        value={filterOptions.rarities.map(x => x.optionValue)}
        negated={negatedOptions(filterOptions.rarities)}
        onChange={handleMultiSelectChange('rarities')}
        onNegate={handleNegate('rarities')}
        chipOnDelete={handleChipDelete('rarities')}
      />
    </Box>
  )
}