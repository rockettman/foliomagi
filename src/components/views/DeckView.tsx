import React from 'react'

import { LessonType } from 'src/cards'
import { CardId, Deck, LessonCounts } from 'src/models'
import { SearchIndexContext } from 'src/contexts/searchIndex'
import TabLayout from 'src/components/layouts/TabLayout'
import { ActiveCardList } from 'src/pages/deck/[username]/[deckid]'
import ChooseCardModal from 'src/components/views/ChooseCardModal'
import useToggle from 'src/hooks/useToggle'
import SquareCardImage from 'src/components/SquareCardImage'
import CardTable from 'src/components/views/CardTable'
import LessonSlider from 'src/components/LessonSlider'

import Box from '@mui/material/Box'
import Stack from '@mui/material/Stack'
import Divider from '@mui/material/Divider'


export default function DeckView(props: {
  deck: Deck,
  onChangeStartingCharacter: (id: CardId) => void,
  onSetLessonCounts: (lessons: LessonCounts) => void,
  onDeleteCard: (id: CardId) => void,
  onDeleteSideboardCard: (id: CardId) => void,
  setActiveCardList: React.Dispatch<React.SetStateAction<ActiveCardList>>
}) {
  const {
    deck,
    onChangeStartingCharacter,
    onSetLessonCounts,
    onDeleteCard,
    onDeleteSideboardCard,
    setActiveCardList
  } = props
  // TODO: use callbacks

  const {cardCatalog} = React.useContext(SearchIndexContext)

  const [
    characterModalIsOpen,
    openCharacterModal,
    closeCharacterModal
  ] = useToggle()

  const chooseStartingCharacter = (id: CardId) => {
    onChangeStartingCharacter(id)
    closeCharacterModal()
  }

  const StartingCharacters = React.useMemo(() => {
    return cardCatalog ? cardCatalog.startingCharacters : []
  }, [cardCatalog])

  const startingCharacter = React.useMemo(() => {
    if (deck && cardCatalog) {
      return cardCatalog.cardIdMap[deck.startingCharacter]
    }
  }, [deck, cardCatalog])

  const LessonCountMap: {[key in LessonType]: number} | undefined = React.useMemo(() => {
    if (deck) {
      return {
        "Care of Magical Creatures": deck ? deck.lessons.careOfMagicalCreatures : 0,
        Charms: deck ? deck.lessons.charms : 0,
        Potions: deck ? deck.lessons.potions : 0,
        Quidditch: deck ? deck.lessons.quidditch : 0,
        Transfiguration: deck ? deck.lessons.transfiguration : 0
      }
    }
  }, [deck])

  const SetLessonCountMap: {[key in LessonType]: (newCount: number) => LessonCounts} | undefined = React.useMemo(() => {
    if (deck) {
      return {
        "Care of Magical Creatures": c => ({...deck.lessons, careOfMagicalCreatures: c}),
        Charms: c => ({...deck.lessons, charms: c}),
        Potions: c => ({...deck.lessons, potions: c}),
        Quidditch: c => ({...deck.lessons, quidditch: c}),
        Transfiguration: c => ({...deck.lessons, transfiguration: c})
      }
    }
  }, [deck])

  const changeLessonCount = (lesson: LessonType) => {
    return (newValue: number) => {
      if (SetLessonCountMap) {
        const newLessonCounts = SetLessonCountMap[lesson](newValue)
        onSetLessonCounts(newLessonCounts)
      }
    }
  }

  const cardList = React.useMemo(() => {
    return deck ? deck.cards : []
  }, [deck])

  const sideboard = React.useMemo(() => {
    return deck ? deck.sideboard : []
  }, [deck])

  return (
    <React.Fragment>
      <Box id='deck-view'
        sx={{
          maxHeight: '100%',
          overflow: 'auto',
          padding: (theme) => theme.spacing(2)
        }}
      >
        <Stack direction='row' alignItems='stretch' justifyContent='space-between'>
          <SquareCardImage
            card={startingCharacter}
            onClick={openCharacterModal}
            priority={true}
            sx={{
              width: '40%',
              alignSelf: 'center'
            }}
          />
          <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            flex: 1,
            minHeight: '140px',
          }}>
            <Stack
              direction='row'
              alignItems='stretch'
              justifyContent='center'
              spacing={{ xs: 0, sm: 3, md: 0 }}
              paddingY={{ xs: 1, sm: 3, md: 1 }}
            >
              {Object.values(LessonType).map(lesson => (
                <LessonSlider
                  key={`lesson-slider-${lesson}`}
                  lessonType={lesson as LessonType}
                  currentValue={LessonCountMap ? LessonCountMap[lesson as LessonType] : 0}
                  onChange={changeLessonCount(lesson as LessonType)}
                />
              ))}
            </Stack>
          </Box>
        </Stack>

        <Divider />

        <Box sx={{
          paddingTop: (theme) => theme.spacing(2)
        }}>
          <TabLayout
            label='deck-cards-table'
            tabs={[
              {
                label: 'Cards',
                contents: <CardTable cardCounts={cardList} />
              },
              {
                label: 'Sideboard',
                contents: <CardTable cardCounts={sideboard} />
              }
            ]}
            onTabChange={activeTab => {
              activeTab == 0 ? setActiveCardList('main') : setActiveCardList('sideboard')
            }}
          />
        </Box>
      </Box>
      <ChooseCardModal
        cards={StartingCharacters}
        currentSelection={deck.startingCharacter as CardId}
        open={characterModalIsOpen}
        onSelect={chooseStartingCharacter}
        onClose={closeCharacterModal}
      />
    </React.Fragment>
  )
}
