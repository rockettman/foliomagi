import React from 'react'

import {
  Card,
  DefaultSortOrder,
  EmptyFilterOptions,
  filterCards,
  FilterOptions,
  sortCards,
  SortOrder
} from 'src/cards'
import { SearchIndexContext } from 'src/contexts/searchIndex'
import SearchToolbar from 'src/components/views/SearchToolbar'
import AdvancedSearchOptions from 'src/components/views/AdvancedSearchOptions'
import FilterOptionsMenu from 'src/components/views/FilterOptionsMenu'
import SortOptionsMenu from 'src/components/views/SortOptionsMenu'
import CardList from 'src/components/views/CardList'
import ToolbarLayout from 'src/components/layouts/ToolbarLayout'
import AdaptiveDrawerLayout from 'src/components/layouts/AdaptiveDrawerLayout'
import { CardListUnderbar } from 'src/components/CardListUnderbars'
import CardListModal from 'src/components/views/CardListModal'
import { CardId } from 'src/models'
import useToggle from 'src/hooks/useToggle'

import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import SettingsIcon from '@mui/icons-material/Settings';
import { useScrollTrigger } from '@mui/material'

export default function CardSearch(props: {
  actionHook?: JSX.Element,
  cardListUnderbar?: CardListUnderbar
}) {

  const {cardCatalog} = React.useContext(SearchIndexContext)

  const [cards, setCards] = React.useState<Card[]>([])
  const [filterOptions, setFilterOptions] = React.useState<FilterOptions>(EmptyFilterOptions)
  const [sortOrder, setSortOrder] = React.useState<SortOrder>(DefaultSortOrder)

  const [
    advancedOptionsMenuIsOpen,
    openAdvancedOptionsMenu,
    closeAdvancedOptionsMenu
  ] = useToggle()

  const [cardModalState, setCardModalState] = React.useState({
    open: false,
    initialCard: CardId.B9
  })

  React.useEffect(() => {
    if (cardCatalog !== undefined) {
      setCards(cardCatalog.allCards())
    }
  }, [cardCatalog])

  const [scrollTarget, setScrollTarget] = React.useState<Node | Window | undefined>()
  const scrollTrigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: scrollTarget
  });

  const filteredAndSortedCards = sortCards(
    filterCards(cards, filterOptions),
    sortOrder
  )

  const AdvancedOptionsMenu = (
    <AdvancedSearchOptions
      filterMenu={
        <FilterOptionsMenu
          cards={filteredAndSortedCards}
          filterOptions={filterOptions}
          setFilterOptions={setFilterOptions}
        />
      }
      sortMenu={
        <SortOptionsMenu
          sortOrder={sortOrder}
          setSortOrder={setSortOrder}
        />
      }
      closeCallback={closeAdvancedOptionsMenu}
      onReset={() => {
        setFilterOptions(EmptyFilterOptions)
        setSortOrder(DefaultSortOrder)
      }}
    />
  )

  return (
    <React.Fragment>
      <AdaptiveDrawerLayout
        label='card-search-layout'
        anchor='right'
        open={advancedOptionsMenuIsOpen}
        drawerContents={AdvancedOptionsMenu}
        onClose={closeAdvancedOptionsMenu}
      >
        <ToolbarLayout
          label='card-search'
          toolbar={
            <SearchToolbar
              placeholder='Search Cards...'
              catalog={cardCatalog}
              resultsCallback={setCards}
              leftAction={props.actionHook}
              rightAction={advancedOptionsMenuIsOpen ? undefined :
                <IconButton onClick={openAdvancedOptionsMenu}>
                  <SettingsIcon />
                </IconButton>
              }
              elevate={scrollTrigger}
            />
          }
        >
          <Box id='card-list-container'
            ref={(node: Node) => {
              if (node) {
                setScrollTarget(node);
              }
            }}
            sx={{
              width: '100%',
              maxHeight: '100%',
              overflow: 'auto',
              padding: (theme) => theme.spacing(2)
            }}
          >
            <CardList
              cards={filteredAndSortedCards}
              underbar={props.cardListUnderbar}
              onCardClick={cardId => setCardModalState({
                open: true,
                initialCard: cardId
              })}
            />
          </Box>
        </ToolbarLayout>
      </AdaptiveDrawerLayout>
      <CardListModal
        cards={filteredAndSortedCards}
        initialCardShown={cardModalState.initialCard}
        open={cardModalState.open}
        onClose={() => setCardModalState({
          ...cardModalState,
          open: false
        })}
        actions={props.cardListUnderbar}
      />
    </React.Fragment>
  )
}