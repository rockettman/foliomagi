import React from 'react'

import DeckList from 'src/components/views/DeckList'
import { Deck } from 'src/models'

import Box from '@mui/material/Box'

export default function DeckSearch(props: {
  decks: Deck[]
}) {
  const {decks} = props

  return (
    <Box id='deck-list-container'
      sx={{
        width: '100%',
        maxHeight: '100%',
        overflow: 'auto',
        padding: (theme) => theme.spacing(2)
      }}
    >
      <DeckList decks={decks} />
    </Box>
  )
}