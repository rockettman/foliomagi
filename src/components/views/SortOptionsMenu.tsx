import React from 'react';

import { DefaultSortOrder, SortField, SortOrder } from 'src/cards';

import { useTheme } from '@mui/material/styles';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItem from '@mui/material/ListItem';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ListItemIcon from '@mui/material/ListItemIcon';
import IconButton from '@mui/material/IconButton';
import ListItemText from '@mui/material/ListItemText';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import Stack from '@mui/material/Stack';

const ReversibleSortFields: SortField[] = [
  'Power Cost',
  'Provides Power Amount',
  'Creature Damage',
  'Creature Health'
]

export default function SortOptionsMenu(props: {
  sortOrder: SortOrder,
  setSortOrder: (newSortOrder: SortOrder) => void
}) {

  const theme = useTheme()

  const {sortOrder, setSortOrder} = props
  const sortFields = Object.keys(SortField)

  function handleFieldClicked(clicked: SortField) {
    return () => {
      if (sortOrder.fields.includes(clicked)) {
        setSortOrder({
          ...sortOrder,
          fields: sortOrder.fields.filter(field => field !== clicked)
        })
      } else {
        setSortOrder({
          ...sortOrder,
          fields: [...sortOrder.fields, clicked]
        })
      }
    }
  }

  function arrayEquals<T>(a: T[], b: T[]): boolean {
    return a.length == b.length && a.every((val, index) => val === b[index])
  }

  function sortOrderEquals(a: SortOrder, b: SortOrder): boolean {
    return (
      arrayEquals(a.fields, b.fields) &&
      a.reversed.every(value => b.reversed.includes(value))
    )
  }

  const SortOptionListItem = (field: SortField) => {
    const reversed = sortOrder.reversed.includes(field)
    return (
      <ListItem
        key={field}
        disablePadding
        secondaryAction={
          ReversibleSortFields.includes(field) &&
          sortOrder.fields.includes(field) &&
          <Stack direction='row'>
            <IconButton
              onClick={() => setSortOrder({
                ...sortOrder,
                reversed: sortOrder.reversed.filter(x => x !== field)
              })}
              sx={{ padding: 0 }}
            >
              <ArrowUpwardIcon style={{
                color: reversed ? 'grey' : theme.palette.secondary.main
              }} />
            </IconButton>
            <IconButton
              onClick={() => setSortOrder({
                ...sortOrder,
                reversed: [...sortOrder.reversed, field]
              })}
              sx={{ padding: 0 }}
            >
              <ArrowDownwardIcon style={{
                color: reversed ? theme.palette.secondary.main : 'grey'
              }} />
            </IconButton>
          </Stack>
        }
      >
        <ListItemButton
          onClick={handleFieldClicked(field)}
          selected={sortOrder.fields.includes(field)}
        >
          <Stack direction='row'>
          <ListItemIcon
            sx={{
              minWidth: '10px',
              marginRight: (theme) => theme.spacing(1),
              alignItems: 'center'
            }}
          >
            {sortOrder.fields.indexOf(field) != -1 ?
              <Typography>{sortOrder.fields.indexOf(field) + 1}</Typography> :
              <Typography></Typography>
            }
          </ListItemIcon>
          <ListItemText primary={field}
            sx={{ flexShrink: 1 }}
          />
          </Stack>
        </ListItemButton>
      </ListItem>
    )
  }

  return (
    <Box sx={{
      maxHeight: '100%',
      overflow: 'auto',
      padding: (theme) => theme.spacing()
    }}>
      <List>
        {sortOrder.fields.map(SortOptionListItem)}
        {
          sortFields.filter(x => !sortOrder.fields.includes(x as SortField))
            .map(x => SortOptionListItem(x as SortField))
        }
      </List>
      <Stack direction='row'>
        <Button
          fullWidth
          variant={sortOrderEquals(sortOrder, DefaultSortOrder) ?
            'contained' : 'outlined'
          }
          onClick={() => setSortOrder(DefaultSortOrder)}
        >
          Default
        </Button>
        <Button
          fullWidth
          variant='text'
          onClick={() => setSortOrder({fields: [], reversed: []})}>
          Clear
        </Button>
      </Stack>
    </Box>
  )
}