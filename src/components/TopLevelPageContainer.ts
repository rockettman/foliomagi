
import { styled } from "@mui/material/styles"
import Box from "@mui/material/Box"

const TopLevelPageContiner = styled(Box)({
  position: 'fixed',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0
})

export default TopLevelPageContiner
