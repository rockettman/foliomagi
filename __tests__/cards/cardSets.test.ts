import { cardSets, HPTCGSetName } from "src/cards";

test('cardSets has the right number of sets', () => {
  expect(cardSets.length).toBe(6);
})

test('each set has the right number of cards', () => {
  cardSets.forEach(set => {
    let expected = set.totalCards

    if (set.setName === HPTCGSetName.Base) {
      expected = +expected + 2
    }

    expect(set.cards.length).toBe(expected)
  })
})