(Describe your idea, feedback, question for the community, etc)

(If your idea is best described as a feature proposal, please create a [Feature Proposal](https://gitlab.com/rockettman-labs/foliomagi/-/issues/new?issuable_template=Feature%20Proposal) instead)


/label ~"type::discussion" ~"status::triage"
/cc @rockettman
