# FolioMagi

https://foliomagi.cards

FolioMagi is an app for lovers of the Harry Potter Trading Card Game.

---

## Features
- card search
- deck building and saving

## Roadmap
- register your personal card collection
- view and register public decks
- play a game with one of your decks or a public deck against
    - another user
    - AI player

## Contributing
Pull requests are welcome. Head over to our [Backlog](https://gitlab.com/rockettman/foliomagi/-/issues?scope=all&state=opened&label_name[]=status%3A%3Abacklog) to browse and pick up any issue.

For changes without an issue, please [create a Feature Proposal](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Feature%20Proposal) first so the community can weigh in before you spend all that time and energy.

See [CONTRIBUTING](./CONTRIBUTING.md) for complete instructions on dev environment setup, deployment, testing, and submitting changes.

### Found a bug?
Please [submit a bug report](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Bug)

## Code of Conduct
We're all Harry Potter fans here, so we know that LOVE > hate ;)

See our [Code of Conduct](./CODE_OF_CONDUCT.md) for specifics.

## License
[GNU GPLv3](./LICENSE)
