
describe('Search Page', () => {
  beforeEach(() => {
    cy.visit('/search')
  })

  it('should have a title', () => {
    cy.title().should('eq', 'FolioMagi');
  })

})
