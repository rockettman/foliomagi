
describe('Decks Page', () => {
  beforeEach(() => {
    cy.visit('/decks')
  })

  it('should have a title', () => {
    cy.title().should('eq', 'FolioMagi');
  })

})
