
describe('Home Page', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should have a title', () => {
    cy.title().should('eq', 'FolioMagi');
  })

})
