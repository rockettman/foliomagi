# Contributing

Contributions are welcome whether you are an HPTCG nut who wants to make this app better or just someone looking for a React/AWS app to work on.

If you're a beginner and nervous about jumping in, don't fret, come on in the water's warm!

1. [Getting Started](#start)
    1. [Fork Repository](#fork-repo)
    2. [Install Packages](#install)
    3. [Deploy Backend](#deploy)
    4. [Run](#run)
    5. [Test](#test)
2. [Making Changes](#changes)
    1. [Find issue to work on](#find-issue)
    2. [Claim issue](#claim-issue)
    3. [Work on issue](#work-on-issue)
3. [Label Schema](#label-schema)
4. [Issue Templates](#issue-templates)

---

## Getting Started <a name='start'></a>

FolioMagi is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

### Fork Repository <a name='fork-repo'></a>

1. [Fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork) the FolioMagi repo
2. Set up your fork to [mirror](https://docs.gitlab.com/ee/user/project/repository/mirror/index.html) the main repo so it stays in sync with any new changes

Clone your forked repository locally:

```bash
git clone <forked repository url>
cd foliomagi
```

### Install Packages <a name='install'></a>

Install `npm` packages:

```bash
npm install
```

Install and configure the `amplify` cli:

```bash
npm install -g @aws-amplify/cli
amplify configure
```

### Deploy Backend <a name='deploy'></a>

FolioMagi is set up to deploy on AWS via [Amplify](https://docs.amplify.aws/).

Read the [Amplify docs for Nextjs hosting](https://docs.amplify.aws/guides/hosting/nextjs/q/platform/js/) to learn more.

Initialize Amplify in your local repo with the following options:

```bash
$ amplify init

? Enter a name for the project: MyFolioMagi
? Enter a name for the environment: dev
? Choose your default editor: Visual Studio Code (or your preferred editor)
? Choose the type of app that youre building: javascript
? What javascript framework are you using: react
? Source Directory Path: src
? Distribution Directory Path: .next
? Build Command: npm run-script build
? Start Command: npm run-script start
? Do you want to use an AWS profile? Y
? Please choose the profile you want to use: <your profile>
```

Create backend environment:

```bash
amplify push
```

You can push local changes to your AWS account anytime by running the above command again.

### Run <a name='run'></a>

Start local development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing pages by modifying files in the `src/pages/` folder. The page auto-updates as you edit the file.

The `src/pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

### Testing <a name='test'></a>

Run all tests:

```bash
npm run test
```

Run jest unit tests only:

```bash
npm run test:unit
```

Run cypress e2e tests only:

```bash
npm run test:e2e
```

The npm script that runs just the cypress tests does not run against the dev server so remember to build the application before running the cypress tests on their own:

```bash
npm run build
```

It's also good to make sure the production app starts before submitting a pull request:

```bash
npm run build && npm run start
```

---

## Making Changes <a name='changes'></a>

### Find issue to work on <a name='find-issue'></a>

Issues that are ready to be worked on will be marked with the `status::backlog` label.

Head over to our [Backlog](https://gitlab.com/rockettman/foliomagi/-/issues?scope=all&state=opened&label_name[]=status%3A%3Abacklog) to browse issues ready to get worked on.

See our [Label Schema](#label-schema) for a list of issue types.

### Claim issue <a name='claim-issue'></a>

In the right panel of the Issue, edit the `Assignee` field and select yourself.

If you do not see yourself or don't have the option to edit the `Assignee` field, simply comment on the issue saying you'd like to work on it.

### Work on issue <a name='work-on-issue'></a>

1. Create a new branch in your [forked repo](#fork-repo) where you'll make changes
2. [Open a Merge Request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) with "WIP" at the start of the title, your new branch as the source, and the main repo's `dev` branch as the target
3. Make your changes and commit them to your branch
4. Request a review when you are ready

---

## Label Schema <a name='label-schema'></a>

This repo uses two main groups of labels: `type` and `status`.

Each issue can have at most one type and one status at a time.

`type` prioritized top to bottom:
- `ops-ticket`: Auto-cut ops ticket created by alarm
- `support`: User needs help
- `bug`: Something is not working as intended
- `maintenance`: Operations and ops automation tasks
- `question`: Question or help request from the dev community
- `enhancement`: New feature or change to existing functionality
- `documentation`: Changes to the Readme, Contributing, etc
- `discussion`: Community discussion about a proposed change or major decision

`status` lifecycle top to bottom:
- `triage`
- `backlog`
- `in-progress`
- `blocked`
- `merged` or `discarded`
- `deployed`

---

## Issue Templates <a name='issue-templates'></a>

[Support Request](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Support%20Request)

[Bug](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Bug)

[Maintenance Task](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Maintenance%20Task)

[Question](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Question)

[Feature Proposal](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Feature%20Proposal)

[Enhancement](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Enhancement)

[Documentation Task](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Documentation%20Task)

[Discussion](https://gitlab.com/rockettman/foliomagi/-/issues/new?issuable_template=Discussion)
