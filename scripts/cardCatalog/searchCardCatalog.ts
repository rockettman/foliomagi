import { CardCatalog } from "src/cards";
import { asyncTimeIt } from "./utils";


export default async function searchCatalog(query: string) {
  const results = await asyncTimeIt(() => {
    return _searchCatalog(query)
  })
  console.log(`Entire time: ${results.executionMillis}ms`)
}

async function _searchCatalog(query: string) {
  const catalog = new CardCatalog();

  console.log('Hydrating card catalog...')
  const hydrateResults = await asyncTimeIt(() => {
    return catalog.hydrate();
  })
  console.log(`Took ${hydrateResults.executionMillis}ms to index cards`)

  console.log(`Searching for ${query}...`)
  const searchResults = await asyncTimeIt(() => {
    return catalog.search(query)
  })
  console.log(JSON.stringify(searchResults.funcResponse, null, 2))
  console.log(`Search took ${searchResults.executionMillis}ms`)
}